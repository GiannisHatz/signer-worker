import { ExpressDataApplication, serviceRouter } from '@themost/express';
import express, { Application } from 'express';
import request from 'supertest';
import { SignerWorker } from '../src/index';
import path from 'path';
import passport from 'passport';
import BearerStrategy from 'passport-http-bearer';

const TEST_BEARER = '2skQ9Dq5JMzPkENa9ax9Pu5pwTSgdPBq78UVsp6nfHqdsTo7';

describe('SignerWorker', () => {

    let app: Application;
    beforeAll((done) => {
        app = express();
        passport.use(new BearerStrategy({
            session: false,
            passReqToCallback: true
          }, function (req: Express.Request, token: string, done: (err?: any, result?: any) => void) {
              return done(null, {
                name: 'user',
                authenticationProviderKey: 1,
                authenticationType:'Bearer',
                authenticationToken: token,
                authenticationScope: 'profile'
              });
        }));
        const dataApplication = new ExpressDataApplication();
        app.set(ExpressDataApplication.name, dataApplication);
        app.use(passport.initialize());
        app.use(dataApplication.middleware(app));
        app.use('/api', passport.authenticate('bearer', { session: false }), serviceRouter);
        return done();
    });
    it('should use as service', async () => {
        const dataApp: ExpressDataApplication = app.get(ExpressDataApplication.name);
        dataApp.useService(SignerWorker as any);
        let response = await request(app)
            .get('/api/signer');
        expect(response.status).toBe(401);
        response = await request(app)
            .get('/api/signer').set('Authorization', `Bearer ${TEST_BEARER}`);
        expect(response.status).toBe(200);
    });

    it('should handle unsupported methods', async () => {
        const dataApp: ExpressDataApplication = app.get(ExpressDataApplication.name);
        let response = await request(app)
            .post('/api/signer/sign')
            .set('Authorization', `Bearer ${TEST_BEARER}`)
            .attach('file',path.resolve(__dirname, './files/unsigned.pdf'));;
        expect(response.status).toBe(405);
    });

    it('should verify document', async () => {
        const dataApp: ExpressDataApplication = app.get(ExpressDataApplication.name);
        let response = await request(app)
            .post('/api/signer/verify')
            .set('Authorization', `Bearer ${TEST_BEARER}`)
            .attach('file',path.resolve(__dirname, './files/signed.pdf'));;
        expect(response.status).toBe(200);
        expect(response.body).toBeTruthy();
        expect(response.body).toBeInstanceOf(Array);
        let verificationResult = response.body[0];
        expect(verificationResult).toBeTruthy();
        expect(verificationResult.valid).toBeTrue();

        response = await request(app)
            .post('/api/signer/verify')
            .set('Authorization', `Bearer ${TEST_BEARER}`)
            .attach('file',path.resolve(__dirname, './files/unsigned.pdf'));
        expect(response.status).toBe(200);
        expect(response.body).toBeTruthy();
        expect(response.body).toBeInstanceOf(Array);
        verificationResult = response.body[0];
        expect(verificationResult).toBeFalsy();
    });

    it('should inspect signature position', async () => {
        const dataApp: ExpressDataApplication = app.get(ExpressDataApplication.name);
        const response = await request(app)
            .post('/api/signer/signature/inspect')
            .set('Authorization', `Bearer ${TEST_BEARER}`)
            .field('width', 300)
            .field('height', 80)
            .field('textPosition', 'center bottom')
            .field('text', '(Signature)')
            .attach('file',path.resolve(__dirname, './files/unsigned.pdf'));;
        expect(response.status).toBe(200);
        const inspectResult = response.body;
        expect(inspectResult).toBeTruthy();
        expect(inspectResult.page).toBe(1);
        expect(inspectResult.position).toBeInstanceOf(Array);
    });

    it('should embed signature line', async () => {
        const dataApp: ExpressDataApplication = app.get(ExpressDataApplication.name);
        const response = await request(app)
            .post('/api/signer/signature/embed')
            .set('Authorization', `Bearer ${TEST_BEARER}`)
            .attach('file',path.resolve(__dirname, './files/workbook.xlsx'));;
        expect(response.status).toBe(200);
    });

});