export const SignerScopeAccess = [
    {
        scope: [
            'registrar'
        ],
        resource: '/api/signer/?$',
        access: [
            'read',
            'write'
        ]
    },
    {
        scope: [
            'teachers'
        ],
        resource: '/api/signer/?$',
        access: [
            'read',
            'write'
        ]
    }
]