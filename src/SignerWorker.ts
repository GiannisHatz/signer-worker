import { ApplicationBase, ApplicationService, HttpForbiddenError } from '@themost/common';
import { BehaviorSubject } from 'rxjs';
import { Application, RequestHandler, Router } from 'express';
import { signerWorkerRouter } from './signerWorkerRouter';
import { SignerScopeAccess } from './SignerScopeAccess';
import { serviceRouter } from '@themost/express';

declare interface ApplicationBaseWithContainer extends ApplicationBase {
    container?: BehaviorSubject<Application>;
}

function insertRouterAfter(parent: Router, after: any, insert: any) {
    const afterIndex = parent.stack.findIndex( (item) => {
        return item === after;
    });
    if (afterIndex < 0) {
        throw new Error('Target router cannot be found in parent stack.');
    }
    const findIndex = parent.stack.findIndex( (item) => {
        return item === insert;
    });
    if (findIndex < 0) {
        throw new Error('Router to be inserted cannot be found in parent stack.');
    }
    // remove last router
    parent.stack.splice(findIndex, 1);
    // move up
    parent.stack.splice(afterIndex + 1, 0, insert);
}

function insertRouterBefore(parent: Router, before: any, insert: any) {
    const beforeIndex = parent.stack.findIndex( (item) => {
        return item === before;
    });
    if (beforeIndex < 0) {
        throw new Error('Target router cannot be found in parent stack.');
    }
    const findIndex = parent.stack.findIndex( (item) => {
        return item === insert;
    });
    if (findIndex < 0) {
        throw new Error('Router to be inserted cannot be found in parent stack.');
    }
    // remove last router
    parent.stack.splice(findIndex, 1);
    // move up
    parent.stack.splice(beforeIndex, 0, insert);
}

class ScopeAccessConfiguration {
    public elements = [];
}

/**
 * Validates user authentication scopes against requested resource based on current application configuration
 * @returns {Function}
 */
 function validateScope(): RequestHandler {
    return (req: any, res: any, next: any) => {
        const scopeAccessConfiguration = req.context.getApplication().getConfiguration().getStrategy(ScopeAccessConfiguration);
        if (scopeAccessConfiguration == null) {
            return next();
        }
        scopeAccessConfiguration.verify(req).then((value: any) => {
            if (value) {
                return next();
            }
            return next(new HttpForbiddenError('Access denied due to authorization scopes.'))
        }).catch((reason: Error) => {
            return next(reason);
        });
    };
}

export class SignerWorker extends ApplicationService {
    constructor(app: ApplicationBase) {
        super(app);
        const thisApp = app as ApplicationBaseWithContainer;
        if (thisApp && thisApp.container) {
            thisApp.container.subscribe((container: Application) => {
                if (container) {
                    // add scope access
                    const scopeAccess = app.getConfiguration().getStrategy(ScopeAccessConfiguration);
                    if (scopeAccess) {
                        scopeAccess.elements.push.apply(scopeAccess.elements, SignerScopeAccess);
                    }
                    // use signer worker router (for authenticated users)
                    serviceRouter.use('/signer/', signerWorkerRouter());
                }
            });
        }
    }
}